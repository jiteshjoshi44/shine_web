$(function () {

	var siteSticky = function () {
		$(".js-sticky-header").sticky({
			topSpacing: 0
		});
	};
	siteSticky();

	var siteMenuClone = function () {

		$('.js-clone-nav').each(function () {
			var $this = $(this);
			$this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
		});


		setTimeout(function () {

			var counter = 0;
			$('.site-mobile-menu .has-children').each(function () {
				var $this = $(this);

				$this.prepend('<span class="arrow-collapse collapsed">');

				$this.find('.arrow-collapse').attr({
					'data-toggle': 'collapse',
					'data-target': '#collapseItem' + counter,
				});

				$this.find('> ul').attr({
					'class': 'collapse',
					'id': 'collapseItem' + counter,
				});

				counter++;

			});

		}, 1000);

		$('body').on('click', '.arrow-collapse', function (e) {
			var $this = $(this);
			if ($this.closest('li').find('.collapse').hasClass('show')) {
				$this.removeClass('active');
			} else {
				$(".arrow-collapse").removeClass('active');
				$(".arrow-collapse").closest('li').find('.collapse').removeClass('show');
				$(".arrow-collapse").addClass('collapsed');
				$this.addClass('active');
			}
			e.preventDefault();

		});

		$(window).resize(function () {
			var $this = $(this),
				w = $this.width();

			if (w > 768) {
				if ($('body').hasClass('offcanvas-menu')) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		})

		$('body').on('click', '.js-menu-toggle', function (e) {
			var $this = $(this);
			e.preventDefault();

			if ($('body').hasClass('offcanvas-menu')) {
				$('body').removeClass('offcanvas-menu');
				$this.removeClass('active');
			} else {
				$('body').addClass('offcanvas-menu');
				$this.addClass('active');
			}
		})

		// click outisde offcanvas
		$(document).mouseup(function (e) {
			var container = $(".site-mobile-menu");
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				if ($('body').hasClass('offcanvas-menu')) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		});
	};
	siteMenuClone();

});



;
(function ($) {
	'use strict'

	$.fn.LineProgressbar = function (options) {
		options = $.extend({
			percentage: 100,
			ShowProgressCount: true,
			duration: 1000,
			unit: '%',
			animation: true,

			// Styling Options
			fillBackgroundColor: '@include get-color(background-color, primary-color-blue);',
			backgroundColor: '#23378512',
			radius: '0px',
			height: '10px',
			width: '85%',
		},
			options
		)

		$.options = options
		return this.each(function (index, el) {
			// Markup
			$(el).html(
				'<div class="progressbar"><div class="proggress"></div><div class="percentCount"></div></div>'
			)

			var progressFill = $(el).find('.proggress')
			var progressBar = $(el).find('.progressbar')

			progressFill.css({
				backgroundColor: options.fillBackgroundColor,
				height: options.height,
				borderRadius: options.radius,
			})
			progressBar.css({
				width: options.width,
				backgroundColor: options.backgroundColor,
				borderRadius: options.radius,
			})

			/**
			 * Progress with animation
			 */
			if (options.animation) {
				// Progressing
				progressFill.animate({
					width: options.percentage + '%',
				}, {
					step: function (x) {
						if (options.ShowProgressCount) {
							$(el)
								.find('.percentCount')
								.text(Math.round(x) + options.unit)
						}
					},
					duration: options.duration,
				})
			} else {
				// Without animation
				progressFill.css('width', options.percentage + '%')
				$(el)
					.find('.percentCount')
					.text(Math.round(options.percentage) + '%')
			}
		})
	}
})(jQuery)

$('[line-progressbar]').each(function () {
	var $this = $(this)

	function LineProgressing() {
		$this.LineProgressbar({
			percentage: $this.data('percentage'),
			unit: $this.data('unit'),
			animation: $this.data('animation'),
			ShowProgressCount: $this.data('showcount'),
			duration: $this.data('duration'),
			fillBackgroundColor: $this.data('progress-color'),
			backgroundColor: $this.data('bg-color'),
			radius: $this.data('radius'),
			height: $this.data('height'),
			width: $this.data('width'),
		})
	}
	var loadOnce = 0
	$this.waypoint(
		function () {
			loadOnce += 1
			if (loadOnce < 2) {
				LineProgressing()
			}
		}, {
		offset: '100%',
		triggerOnce: true
	}
	)
})


$('#mobile').LineProgressbar({
	percentage: 95,
	radius: '5px',
	height: '20px',
});
$('#web').LineProgressbar({
	percentage: 80,
	radius: '5px',
	height: '20px',
});
$('#design').LineProgressbar({
	percentage: 90,
	radius: '5px',
	height: '20px',
});
$('#marketing').LineProgressbar({
	percentage: 60,
	radius: '5px',
	height: '20px',
});


$(document).ready(function ($) {
	var ThemesType = localStorage.getItem('themesmode');
	if (ThemesType == 'dark') {
		$("body").addClass('contrasted');
	} else {
		$("body").removeClass('contrasted');
	}
	$('.counter').counterUp({
		delay: 50,
		time: 2000
	});
});

var $affectedElements = $("body, div, p, span, h1, h2, h3, h4, h5, strong, a, ul, li"); // Can be extended, ex. $("div, p, span.someClass")

$affectedElements.each(function () {
	var $this = $(this);
	$this.data("orig-size", $this.css("font-size"));
});

$("#btn-increase").click(function (event) {
	FontcheckCounter('+');
})
var fontsizecounter = 0;
$("#btn-decrease").click(function (event) {
	FontcheckCounter('-');
})

$("#btn-orig").click(function () {
	fontsizecounter = 0;
	$affectedElements.each(function () {
		$('[style*="font-size"]').css('font-size', '');
	});
})

function changeFontSize(direction) {
	$affectedElements.each(function () {
		var $this = $(this);
		$this.css("font-size", parseInt($this.css("font-size")) + direction);
	});
}

function FontcheckCounter(status) {
	if (status == '+') {
		if (fontsizecounter < 2) {
			fontsizecounter++;
			changeFontSize(1);
		}
	} else {
		if (fontsizecounter > -2) {
			fontsizecounter--;
			changeFontSize(-1);
		}
	}
}

//preloader
$(window).on('load', function () {
	$("#loader").delay(1000).fadeOut(0);
})

$('.scrollMenu')
	.not('[href="#"]')
	.not('[href="#0"]')
	.click(function (event) {
		if (
			location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
			location.hostname == this.hostname
		) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				event.preventDefault();
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000, function () {
					var $target = $(target);
					$target.focus();
					if ($target.is(":focus")) {
						return false;
					} else {
						$target.attr('tabindex', '-1');
						$target.focus();
					};
				});
			}
		}
	});



var scrollToTopButton = document.getElementById('js-top');

var scrollFunc = () => {
	let y = window.scrollY;

	if (y > 0) {
		scrollToTopButton.className = "top-link show";
	} else {
		scrollToTopButton.className = "top-link hide";
	}
};

window.addEventListener("scroll", scrollFunc);

var scrollToTop = () => {
	var c = document.documentElement.scrollTop || document.body.scrollTop;
	if (c > 0) {
		window.requestAnimationFrame(scrollToTop);
		window.scrollTo(0, c - c / 10);
	}
};

scrollToTopButton.onclick = function (e) {
	e.preventDefault();
	scrollToTop();
}

$('body').find('*').each(function () {
	var color = window.getComputedStyle($(this).get(0), null).backgroundColor;
	if ((color.toLowerCase() == "white") ||
		(color.toLowerCase() == "#ffffff") ||
		(color.toLowerCase() == "rgb(255,255,255)")) {
		$(this).css("background-color", "black");
	}
});

$('#darkMode').click(function () {
	$("body").addClass('contrasted');
	localStorage.setItem('themesmode', 'dark');
});

$('#lightMode').click(function () {
	$("body").removeClass('contrasted');
	localStorage.setItem('themesmode', 'light');
});

$().ready(function () {
	$('.tab-title>a').click(function (e) {
		e.preventDefault();
		var index = $(this).parent().index();
		$(this).parent().addClass('active')
			.siblings().removeClass('active')
			.parent('ul.tabs').siblings('.tabs-content').children('.content').removeClass('active')
			.eq(index).addClass('active');
	});
})