var owl = $('.clientCarousel');
owl.owlCarousel({
    items:6,
    loop:true,
    margin:20,
    autoplay:true,
    nav: false,
    dots: false,
    autoplayTimeout:1500,
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:5
        },
        1400:{
            items:6
        }
    }
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[1500])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
});


$('.OwnServices').owlCarousel({
    loop:true,
    margin:20,
    items: 2,
    responsiveClass:true,
    nav: true,
    navText: ["<img src='assets/image/prev.svg' alt='Web app developement'>","<img src='assets/image/next.svg' alt='Mobile app developement'>"],
    dots: false,
    stagePadding: 100,
    responsive:{
        0:{
            items:1,
            stagePadding: 20,
        },
        480:{
            items:1,
            stagePadding: 80,
        },
        600:{
            items:2,
            stagePadding: 30,
        },
        1000:{
            items:2,
        },
        1400:{
            items:3,
        },
        2000:{
            items:5,
        },
        2500:{
            items:6,
        }
    }
});

$('.OwnSolution').owlCarousel({
    loop:true,
    margin:30,
    responsiveClass:true,
    dots: false,
    nav: true,
    autoplay:true,
    autoplayTimeout:1500,
    autoplayHoverPause:true,
    navText: ["<img src='assets/image/prev.svg' alt='Xamarin app developement'>","<img src='assets/image/next.svg' alt='React app developement'>"],
    responsive:{
        0:{
            items:1,
            stagePadding: 50,
            margin:20
        },
        600:{
            items:1,
            stagePadding: 100,
            margin:20
        },
        800:{
            items:2,
            stagePadding: 100,
            margin:20
        },
        1000:{
            items:2,
            stagePadding: 100,
        },
        1400:{
            items:2,
            stagePadding: 200,
        },
        1600:{
            items: 1,
            stagePadding: 600,
        }
    }
});

$('.OwnCase').owlCarousel({
    loop:true,
    margin:20,
    items: 2,
    responsiveClass:true,
    dots: false,
    nav: true,
    // autoplay:true,
    // autoplayTimeout:1500,
    // autoplayHoverPause:true,
    navText: ["<img src='assets/image/prev.svg' alt='iOS app developement'>","<img src='assets/image/next.svg' alt='Android app developement'>"],
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:1,
        },
        700:{
            items:2,
        },
        1000:{
            items:2,
        },
        1400:{
            items:2,
        }
    }
});

$('.OwnTestimonial').owlCarousel({
    loop:true,
    margin:10,
    dots:true,
    dotsData: true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})