$('.ShineSwitcher .ShineSelected').click(function() {
    $('.ShineSwitcher .ShineOption a img').each(function() {
        if (!$(this)[0].hasAttribute('src')) $(this).attr('src', $(this).attr('data-gt-lazy-src'))
    });
    if (!($('.ShineSwitcher .ShineOption').is(':visible'))) {
        $('.ShineSwitcher .ShineOption').stop(true, true).delay(100).slideDown(500);
    }
});
$('.ShineSwitcher .ShineOption').bind('mousewheel', function(e) {
    var ShineOptions = $('.ShineSwitcher .ShineOption');
    if (ShineOptions.is(':visible')) ShineOptions.scrollTop(ShineOptions.scrollTop() - e.originalEvent.wheelDelta);
    return false;
});
$('body').not('.ShineSwitcher').click(function(e) {
    if ($('.ShineSwitcher .ShineOption').is(':visible') && e.target != $('.ShineSwitcher .ShineOption').get()) {
        $('.ShineSwitcher .ShineOption').stop(true, true).delay(100).slideUp(500);
        $('.ShineSwitcher .ShineOption').addClass('d-block');
    }
});

function googleTranslateElementInit() {
    new google.translate.TranslateElement({
        pageLanguage: 'en',
        autoDisplay: false
    }, 'google_translate_element');
}

function GTranslateGetCurrentLang() {
    var keyValue = document['cookie'].match('(^|;) ?googtrans=([^;]*)(;|$)');
    return keyValue ? keyValue[2].split('/')[2] : null;
}

function GTranslateFireEvent(element, event) {
    try {
        if (document.createEventObject) {
            var evt = document.createEventObject();
            element.fireEvent('on' + event, evt)
        } else {
            var evt = document.createEvent('HTMLEvents');
            evt.initEvent(event, true, true);
            element.dispatchEvent(evt)
        }
    } catch (e) {}
}

function doGTranslate(lang_pair) {
    if (lang_pair.value) lang_pair = lang_pair.value;
    if (lang_pair == '') return;
    var lang = lang_pair.split('|')[1];
    if (GTranslateGetCurrentLang() == null && lang == lang_pair.split('|')[0]) return;
    if (typeof ga != 'undefined') {
        ga('send', 'event', 'GTranslate', lang, location.hostname + location.pathname + location.search);
    } else {
        if (typeof _gaq != 'undefined') _gaq.push(['_trackEvent', 'GTranslate', lang, location.hostname + location.pathname + location.search]);
    }
    var teCombo;
    var sel = document.getElementsByTagName('select');
    for (var i = 0; i < sel.length; i++)
        if (/goog-te-combo/.test(sel[i].className)) {
            teCombo = sel[i];
            break;
        } if (document.getElementById('google_translate_element') == null || document.getElementById('google_translate_element').innerHTML.length == 0 || teCombo.length == 0 || teCombo.innerHTML.length == 0) {
        setTimeout(function() {
            doGTranslate(lang_pair)
        }, 500)
    } else {
        teCombo.value = lang;
        GTranslateFireEvent(teCombo, 'change');
        GTranslateFireEvent(teCombo, 'change')
    }
}
if (GTranslateGetCurrentLang() != null) $(document).ready(function() {
    var lang_html = $('div.ShineSwitcher div.ShineOption').find('img[alt="' + GTranslateGetCurrentLang() + '"]').parent().html();
    if (typeof lang_html != 'undefined') $('div.ShineSwitcher div.ShineSelected a').html(lang_html.replace('data-gt-lazy-', ''));
});

$(document).ready(function() {
    var allowed_languages = ["en","fr","ar"];
    var accept_language = navigator.language.toLowerCase() || navigator.userLanguage.toLowerCase();
    switch (accept_language) {
        case 'zh-cn':
            var preferred_language = 'zh-CN';
            break;
        case 'zh':
            var preferred_language = 'zh-CN';
            break;
        case 'zh-tw':
            var preferred_language = 'zh-TW';
            break;
        case 'zh-hk':
            var preferred_language = 'zh-hk';
            break;
        default:
            var preferred_language = accept_language.substr(0, 2);
            break;
    }
    if (preferred_language != 'en' && GTranslateGetCurrentLang() == null && document.cookie.match('gt_auto_switch') == null && allowed_languages.indexOf(preferred_language) >= 0) {
        doGTranslate('en|' + preferred_language);
        document.cookie = 'gt_auto_switch=1; expires=Thu, 05 Dec 2030 08:08:08 UTC; path=/;';
        var lang_html = $('div.ShineSwitcher div.ShineOption').find('img[alt="' + preferred_language + '"]').parent().html();
        if (typeof lang_html != 'undefined') $('div.ShineSwitcher div.ShineSelected a span').html(lang_html.replace('data-gt-lazy-', ''));
    }
});